> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Development

## Vincent Sibley

#### Assignments:

>[A1 Subdirectory](a1/README.md "Assignment 1")
>
>[A2 Subdirectory](a2/README.md "Assignment 2")
>
>[A3 Subdirectory](a3/README.md "Assignment 3")
>
>[A4 Subdirectory](https://bitbucket.org/sibleyv/a4 "Assignment 4")
>

### Projects:

>[Project 1 Subdirectory](p1/README.md "Project 1"))