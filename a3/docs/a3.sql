-- MySQL Script generated by MySQL Workbench
-- 02/17/16 15:32:40
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema vjs11
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `vjs11` ;

-- -----------------------------------------------------
-- Schema vjs11
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `vjs11` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
SHOW WARNINGS;
USE `vjs11` ;

-- -----------------------------------------------------
-- Table `vjs11`.`petstore`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `vjs11`.`petstore` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `vjs11`.`petstore` (
  `pst_id` SMALLINT UNSIGNED NOT NULL,
  `pst_name` VARCHAR(30) NOT NULL,
  `pst_street` VARCHAR(30) NOT NULL,
  `pst_city` VARCHAR(30) NOT NULL,
  `pst_state` CHAR(2) NOT NULL,
  `pst_zip` INT(9) UNSIGNED NOT NULL,
  `pst_phone` BIGINT UNSIGNED NOT NULL,
  `pst_email` VARCHAR(100) NOT NULL,
  `pst_url` VARCHAR(100) NOT NULL,
  `pst_ytd_sales` DECIMAL(10,2) NOT NULL,
  `pst_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pst_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `vjs11`.`customer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `vjs11`.`customer` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `vjs11`.`customer` (
  `cus_id` SMALLINT UNSIGNED NOT NULL,
  `cus_fname` VARCHAR(15) NOT NULL,
  `cus_lname` VARCHAR(30) NOT NULL,
  `cus_street` VARCHAR(30) NOT NULL,
  `cus_city` VARCHAR(30) NOT NULL,
  `cus_state` CHAR(2) NOT NULL,
  `cus_zip` INT UNSIGNED NOT NULL,
  `cus_phone` BIGINT UNSIGNED NOT NULL,
  `cus_email` VARCHAR(100) NOT NULL,
  `cus_balance` DECIMAL(6,2) UNSIGNED NOT NULL,
  `cus_total_sales` DECIMAL(6,2) UNSIGNED NOT NULL,
  `cus_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`cus_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `vjs11`.`pet`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `vjs11`.`pet` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `vjs11`.`pet` (
  `pet_id` SMALLINT UNSIGNED NOT NULL,
  `pst_id` SMALLINT UNSIGNED NOT NULL,
  `cus_id` SMALLINT UNSIGNED NULL,
  `pet_type` VARCHAR(45) NOT NULL,
  `pet_sex` ENUM('m', 'f') NOT NULL,
  `pet_cost` DECIMAL(6,2) UNSIGNED NOT NULL,
  `pet_price` DECIMAL(6,2) UNSIGNED NOT NULL,
  `pet_age` TINYINT UNSIGNED NOT NULL,
  `pet_color` VARCHAR(30) NOT NULL,
  `pet_sale_date` DATE NOT NULL,
  `pet_vaccine` ENUM('y', 'n') NOT NULL,
  `pet_neuter` ENUM('y', 'n') NOT NULL,
  `pet_notes` VARCHAR(225) NULL,
  PRIMARY KEY (`pet_id`),
  INDEX `fk_pet_petstore_idx` (`pst_id` ASC),
  INDEX `fk_pet_customer1_idx` (`cus_id` ASC),
  CONSTRAINT `fk_pet_petstore`
    FOREIGN KEY (`pst_id`)
    REFERENCES `vjs11`.`petstore` (`pst_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pet_customer1`
    FOREIGN KEY (`cus_id`)
    REFERENCES `vjs11`.`customer` (`cus_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `vjs11`.`petstore`
-- -----------------------------------------------------
START TRANSACTION;
USE `vjs11`;
INSERT INTO `vjs11`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (1, 'Apple store', '123 street', 'Atlanta', 'GA', 12345, 1234567890, 'Apple@gmail.com', 'Apple.com', 10000.00, NULL);
INSERT INTO `vjs11`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (2, 'Bear store', '234 street', 'Jacksonville', 'FL', 23456, 1234141255, 'Bear@yahoo.com', 'Bear.com', 20000.00, NULL);
INSERT INTO `vjs11`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (3, 'Cat store', '345 street', 'Miami', 'FL', 34567, 3453241543, 'Cat@hotmail.com', 'Cat.com', 30000.00, NULL);
INSERT INTO `vjs11`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (4, 'Deer store', '456 street', 'Tampa Bay', 'FL', 45678, 5343345433, 'Deer@aol.com', 'Deer.com', 40000.00, NULL);
INSERT INTO `vjs11`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (5, 'Elf store', '567 street', 'Auburn', 'AL', 56789, 2545234534, 'Elf@gmail.com', 'Elf.com', 50000.00, NULL);
INSERT INTO `vjs11`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (6, 'Fin store', '678 street', 'Charlotte', 'NC', 67890, 6353452334, 'Fin@yahoo.com', 'Fin.com', 60000.00, NULL);
INSERT INTO `vjs11`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (7, 'George store', '789 street', 'Richmond', 'VA', 78901, 6342223352, 'George@gmail.com', 'George.com', 70000.00, NULL);
INSERT INTO `vjs11`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (8, 'Huck store', '890 street', 'Seattle', 'WA', 89012, 7534254223, 'Huck@hotmail.com', 'Huck.com', 80000.00, NULL);
INSERT INTO `vjs11`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (9, 'Indian store', '901 street', 'Knoxville', 'TN', 90123, 8654235345, 'Indian@aol.com', 'Indian.com', 90000.00, NULL);
INSERT INTO `vjs11`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (10, 'Jack store', '999 street', 'Dallas', 'TX', 09876, 2131334556, 'Jack@gmail.com', 'Jack.com', 100000.00, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `vjs11`.`customer`
-- -----------------------------------------------------
START TRANSACTION;
USE `vjs11`;
INSERT INTO `vjs11`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (1, 'Vincent', 'Sibley', '123 way', 'Jacksonville', 'FL', 32201, 9023145342, 'Vincent@gmail.com', 100.00, 500.00, NULL);
INSERT INTO `vjs11`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (2, 'Ashlyn', 'Molinaro', '234 way', 'Miami', 'FL', 32201, 1234325367, 'Ashlyn@gmail.com', 150.00, 234.00, NULL);
INSERT INTO `vjs11`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (3, 'Wyatt', 'Dell', '345 way', 'Tampa', 'FL', 32203, 8564655466, 'Wyatt@gmail.com', 200.00, 524.00, NULL);
INSERT INTO `vjs11`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (4, 'Nick', 'Sibley', '456 way', 'Orlando', 'FL', 32204, 4675678888, 'Nick@gmail.com', 250.00, 875.00, NULL);
INSERT INTO `vjs11`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (5, 'Cameron', 'Pennant', '567 way', 'Key West', 'FL', 32205, 8567565777, 'Cam@yahoo.com', 300.00, 325.00, NULL);
INSERT INTO `vjs11`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (6, 'Hunter', 'Hicks', '678 way', 'Ocala', 'FL', 32206, 8756453454, 'Hunter@hotmail.com', 350.00, 545.00, NULL);
INSERT INTO `vjs11`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (7, 'Niko', 'Pappadis', '789 way', 'Tallahassee', 'FL', 32207, 4645524456, 'Niko@aol.com', 400.00, 624.00, NULL);
INSERT INTO `vjs11`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (8, 'Stanko', 'Gutalj', '890 way', 'Pensacola', 'FL', 32208, 7556676446, 'Stanko@gmail.com', 450.00, 522.00, NULL);
INSERT INTO `vjs11`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (9, 'Lina', 'Rojas', '901 way', 'Fort Myers', 'FL', 32209, 8654656666, 'Lina@hotmail.com', 500.00, 113.00, NULL);
INSERT INTO `vjs11`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (10, 'Allen', 'Robinson', '999 way', 'St Augustine', 'FL', 32210, 8747777764, 'Allen@hotmail.com', 550.00, 999.00, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `vjs11`.`pet`
-- -----------------------------------------------------
START TRANSACTION;
USE `vjs11`;
INSERT INTO `vjs11`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (1, 1, 1, 'cat', 'm', 100.00, 250.00, 2, 'black', '2005-01-01', 'y', 'n', NULL);
INSERT INTO `vjs11`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (2, 2, 2, 'dog', 'm', 200.00, 400.00, 2, 'brown', '2006-01-01', 'y', 'n', NULL);
INSERT INTO `vjs11`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (3, 3, 3, 'rabbit', 'f', 300.00, 375.00, 4, 'white', '2007-01-01', 'n', 'y', NULL);
INSERT INTO `vjs11`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (4, 4, 4, 'hamster', 'm', 400.00, 500.00, 1, 'brown', '2008-01-01', 'y', 'n', NULL);
INSERT INTO `vjs11`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (5, 5, 5, 'dog', 'm', 500.00, 600.00, 5, 'black', '2009-01-01', 'n', 'y', NULL);
INSERT INTO `vjs11`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (6, 6, 6, 'dog', 'f', 600.00, 700.00, 3, 'black', '2010-01-01', 'y', 'y', NULL);
INSERT INTO `vjs11`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (7, 7, 7, 'cat', 'f', 700.00, 800.00, 2, 'white', '2011-01-01', 'n', 'n', NULL);
INSERT INTO `vjs11`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (8, 8, 8, 'iguana', 'f', 800.00, 900.00, 2, 'green', '2012-01-01', 'y', 'y', NULL);
INSERT INTO `vjs11`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (9, 9, 9, 'snake', 'f', 900.00, 999.99, 3, 'green', '2013-01-01', 'n', 'y', NULL);
INSERT INTO `vjs11`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (10, 10, 10, 'dog', 'm', 100.00, 200.00, 1, 'black', '2014-01-01', 'y', 'y', NULL);

COMMIT;

