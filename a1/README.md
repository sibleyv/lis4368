> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Development

## Vincent Sibley

### Assignment 1 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Java/JSP/Servlet Development Installation
3. Chapter Question (Chs 1 - 4)

#### README.md file should include the following items:

* Bullet-list items
* Screenshot of running java Hello (#1 above);
* Screenshot of running (http://localhost:9999) (#2 above, Step #4(b) in tutorial);
* git commands w/ short descriptions;
*Bitbucket repo links: a) this assignment and b) the completed tutorial above (bitbucketstationlocations) 

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:
>
>*git init: created an empty Git repository 
>
>*git status: displays the state of the working directory and the staging area
>
>*git add: adds a change in the working directory to the staging area 
>
>*git commit: commits changes to your local repository
>
>*git push: push your local branch to a remote repository
>
>*git pull: fetch from and integrate with another repository or local branch
>
>*git config: Configure the author name and email address to be used with commits

#### Assignment Screenshots:

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)


*Screenshot of running http://localhost:9999*:

![Tomcat Installation Screenshot](img/tomcat.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/sibleyv/bitbucketstationlocations "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/sibleyv/myteamquotes "My Team Quotes Tutorial")
