> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Development

## Vincent Sibley

### Assignment 2 Requirements:


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.


> Assessment: the following links should properly display:

[Tomcat Installation Screenshot](img/tomcat.png)

>[hello] (http://localhost:9999/hello "displays directory, needs index.html")
>
>[Hello Home] (localhhttp://localhost:9999/hello/HelloHome.html "Can rename 'HelloHome.html' to 'index.html'")
>
>[Say Hello] (http://localhost:9999/hello/sayhello "invokes HelloServlet")
>Note: /sayhello maps to HelloServlet.class (changed web.xml file)
>
>[Query book] (http://localhost:9999/hello/querybook.html)
>
>[Say Hi] (http://localhost:9999/hello/sayhi "invokes AnotherHelloServlet")

* Screenshot of the query results from the following link: http://localhost:9999/hello/querybook.html



#### Assignment Screenshots:

*Screenshots*:

![Query Screenshot](img/queryresponse.png)





